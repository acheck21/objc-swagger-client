#import "ACHCheck.h"

@implementation ACHCheck

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"href": @"href", @"documentId": @"documentId", @"clientId": @"clientId", @"individualName": @"individualName", @"companyName": @"companyName", @"accountType": @"accountType", @"amount": @"amount", @"routingNumber": @"routingNumber", @"accountNumber": @"accountNumber", @"checkNumber": @"checkNumber", @"entryClass": @"entryClass", @"entryDescription": @"entryDescription", @"receivedOn": @"receivedOn", @"editedOn": @"editedOn", @"editedBy": @"editedBy", @"deletedOn": @"deletedOn", @"deletedBy": @"deletedBy", @"addenda": @"addenda", @"error": @"error", @"currentState": @"currentState", @"frontImageUrl": @"frontImageUrl", @"rearImageUrl": @"rearImageUrl", @"settlements": @"settlements", @"returns": @"returns", @"attachments": @"attachments" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"href", @"individualName", @"companyName", @"entryDescription", @"editedOn", @"editedBy", @"deletedOn", @"deletedBy", @"addenda", @"error", @"frontImageUrl", @"rearImageUrl", @"settlements", @"returns", @"attachments"];
  return [optionalProperties containsObject:propertyName];
}

@end
