#import "ACHCheckQueryResult.h"

@implementation ACHCheckQueryResult

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"recordsTotal": @"recordsTotal", @"offset": @"offset", @"limit": @"limit", @"first": @"first", @"prev": @"prev", @"next": @"next", @"last": @"last", @"checks": @"checks" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"first", @"prev", @"next", @"last", @"checks"];
  return [optionalProperties containsObject:propertyName];
}

@end
