#import "ACHCreateCheckParams.h"

@implementation ACHCreateCheckParams

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"individualName": @"individualName", @"accountType": @"accountType", @"amount": @"amount", @"routingNumber": @"routingNumber", @"accountNumber": @"accountNumber", @"checkNumber": @"checkNumber", @"micr": @"micr", @"entryClass": @"entryClass", @"addenda": @"addenda", @"frontImageEncoded": @"frontImageEncoded", @"rearImageEncoded": @"rearImageEncoded", @"attachments": @"attachments" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"individualName", @"micr", @"addenda", @"frontImageEncoded", @"rearImageEncoded", @"attachments"];
  return [optionalProperties containsObject:propertyName];
}

@end
