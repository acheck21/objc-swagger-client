#import "ACHReturn.h"

@implementation ACHReturn

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"documentId": @"documentId", @"traceNumber": @"traceNumber", @"amount": @"amount", @"receivedOn": @"receivedOn", @"returnedOn": @"returnedOn", @"returnCode": @"returnCode", @"returnMessage": @"returnMessage" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"traceNumber", @"returnedOn", @"returnCode", @"returnMessage"];
  return [optionalProperties containsObject:propertyName];
}

@end
