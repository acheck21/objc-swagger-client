#import "ACHValidateCheckParams.h"

@implementation ACHValidateCheckParams

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"individualName": @"individualName", @"amount": @"amount", @"routingNumber": @"routingNumber", @"accountNumber": @"accountNumber", @"checkNumber": @"checkNumber", @"micr": @"micr", @"dlNumber": @"dlNumber", @"phoneNumber": @"phoneNumber" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"individualName", @"micr", @"dlNumber", @"phoneNumber"];
  return [optionalProperties containsObject:propertyName];
}

@end
