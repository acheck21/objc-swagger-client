# ACHCheck

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **NSString*** |  | [optional] 
**documentId** | **NSNumber*** |  | 
**clientId** | **NSString*** |  | 
**individualName** | **NSString*** |  | [optional] 
**companyName** | **NSString*** |  | [optional] 
**accountType** | **NSString*** |  | 
**amount** | **NSNumber*** |  | 
**routingNumber** | **NSString*** |  | 
**accountNumber** | **NSString*** |  | 
**checkNumber** | **NSString*** |  | 
**entryClass** | **NSString*** |  | 
**entryDescription** | **NSString*** |  | [optional] 
**receivedOn** | **NSDate*** |  | 
**editedOn** | **NSDate*** |  | [optional] 
**editedBy** | **NSString*** |  | [optional] 
**deletedOn** | **NSDate*** |  | [optional] 
**deletedBy** | **NSString*** |  | [optional] 
**addenda** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**error** | **NSString*** |  | [optional] 
**currentState** | **NSString*** |  | 
**frontImageUrl** | **NSString*** |  | [optional] 
**rearImageUrl** | **NSString*** |  | [optional] 
**settlements** | [**ACHResourceLink***](ACHResourceLink.md) |  | [optional] 
**returns** | [**ACHResourceLink***](ACHResourceLink.md) |  | [optional] 
**attachments** | [**ACHResourceLink***](ACHResourceLink.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


