# ACHCheckQueryResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recordsTotal** | **NSNumber*** |  | 
**offset** | **NSNumber*** |  | 
**limit** | **NSNumber*** |  | 
**first** | [**ACHResourceLink***](ACHResourceLink.md) |  | [optional] 
**prev** | [**ACHResourceLink***](ACHResourceLink.md) |  | [optional] 
**next** | [**ACHResourceLink***](ACHResourceLink.md) |  | [optional] 
**last** | [**ACHResourceLink***](ACHResourceLink.md) |  | [optional] 
**checks** | [**NSArray&lt;ACHCheck&gt;***](ACHCheck.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


