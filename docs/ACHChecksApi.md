# ACHChecksApi

All URIs are relative to *https://gateway.acheck21.com/GlobalGateway*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createCheck**](ACHChecksApi.md#createcheck) | **PUT** /api/v1/checks/{clientId} | Create a new Check transaction.
[**getAttachments**](ACHChecksApi.md#getattachments) | **GET** /api/v1/checks/{clientId}/{documentId}/attachments | Retrieve Attachments for a specific Check. Usually there is at most one settlement per check.
[**getCheck**](ACHChecksApi.md#getcheck) | **GET** /api/v1/checks/{clientId}/{documentId} | Retrieve a specific Check
[**getChecks**](ACHChecksApi.md#getchecks) | **GET** /api/v1/checks/{clientId} | Search for all Checks that belong to a merchant account.
[**getReturns**](ACHChecksApi.md#getreturns) | **GET** /api/v1/checks/{clientId}/{documentId}/returns | Retrieve returns for a specific Check.
[**getSettlements**](ACHChecksApi.md#getsettlements) | **GET** /api/v1/checks/{clientId}/{documentId}/settlements | Retrieve Settlements for a specific Check. Usually there is at most one settlement per check.
[**validateCheck**](ACHChecksApi.md#validatecheck) | **POST** /api/v1/checks/{clientId}/validate | Validates a check. This usually involves verifying the account exists and that there are no unpaid items (a.k.a. bounced checks) associated with the account. This is configured in the Global Gateway&#39;s client manager.
[**voidCheck**](ACHChecksApi.md#voidcheck) | **DELETE** /api/v1/checks/{clientId}/{documentId} | Delete a specific Check transaction.


# **createCheck**
```objc
-(NSNumber*) createCheckWithClientId: (NSString*) clientId
    values: (ACHCreateCheckParams*) values
        completionHandler: (void (^)(ACHCheck* output, NSError* error)) handler;
```

Create a new Check transaction.

### Example 
```objc
ACHConfiguration *apiConfig = [ACHConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: basicAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* clientId = @"clientId_example"; // 
ACHCreateCheckParams* values = [[ACHCreateCheckParams alloc] init]; //  (optional)

ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];

// Create a new Check transaction.
[apiInstance createCheckWithClientId:clientId
              values:values
          completionHandler: ^(ACHCheck* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling ACHChecksApi->createCheck: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***|  | 
 **values** | [**ACHCreateCheckParams***](ACHCreateCheckParams*.md)|  | [optional] 

### Return type

[**ACHCheck***](ACHCheck.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAttachments**
```objc
-(NSNumber*) getAttachmentsWithClientId: (NSString*) clientId
    documentId: (NSNumber*) documentId
        completionHandler: (void (^)(NSArray<ACHAttachment>* output, NSError* error)) handler;
```

Retrieve Attachments for a specific Check. Usually there is at most one settlement per check.

### Example 
```objc
ACHConfiguration *apiConfig = [ACHConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: basicAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* clientId = @"clientId_example"; // 
NSNumber* documentId = @789; // 

ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];

// Retrieve Attachments for a specific Check. Usually there is at most one settlement per check.
[apiInstance getAttachmentsWithClientId:clientId
              documentId:documentId
          completionHandler: ^(NSArray<ACHAttachment>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling ACHChecksApi->getAttachments: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***|  | 
 **documentId** | **NSNumber***|  | 

### Return type

[**NSArray<ACHAttachment>***](ACHAttachment.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getCheck**
```objc
-(NSNumber*) getCheckWithClientId: (NSString*) clientId
    documentId: (NSNumber*) documentId
        completionHandler: (void (^)(ACHCheck* output, NSError* error)) handler;
```

Retrieve a specific Check

### Example 
```objc
ACHConfiguration *apiConfig = [ACHConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: basicAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* clientId = @"clientId_example"; // 
NSNumber* documentId = @789; // 

ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];

// Retrieve a specific Check
[apiInstance getCheckWithClientId:clientId
              documentId:documentId
          completionHandler: ^(ACHCheck* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling ACHChecksApi->getCheck: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***|  | 
 **documentId** | **NSNumber***|  | 

### Return type

[**ACHCheck***](ACHCheck.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getChecks**
```objc
-(NSNumber*) getChecksWithClientId: (NSString*) clientId
    startDate: (NSDate*) startDate
    endDate: (NSDate*) endDate
    currentStates: (NSArray<NSString*>*) currentStates
    offset: (NSNumber*) offset
    limit: (NSNumber*) limit
        completionHandler: (void (^)(ACHCheckQueryResult* output, NSError* error)) handler;
```

Search for all Checks that belong to a merchant account.

### Example 
```objc
ACHConfiguration *apiConfig = [ACHConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: basicAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* clientId = @"clientId_example"; // 
NSDate* startDate = @"2013-10-20T19:20:30+01:00"; //  (optional)
NSDate* endDate = @"2013-10-20T19:20:30+01:00"; //  (optional)
NSArray<NSString*>* currentStates = @[@"currentStates_example"]; //  (optional)
NSNumber* offset = @56; //  (optional)
NSNumber* limit = @56; //  (optional)

ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];

// Search for all Checks that belong to a merchant account.
[apiInstance getChecksWithClientId:clientId
              startDate:startDate
              endDate:endDate
              currentStates:currentStates
              offset:offset
              limit:limit
          completionHandler: ^(ACHCheckQueryResult* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling ACHChecksApi->getChecks: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***|  | 
 **startDate** | **NSDate***|  | [optional] 
 **endDate** | **NSDate***|  | [optional] 
 **currentStates** | [**NSArray&lt;NSString*&gt;***](NSString*.md)|  | [optional] 
 **offset** | **NSNumber***|  | [optional] 
 **limit** | **NSNumber***|  | [optional] 

### Return type

[**ACHCheckQueryResult***](ACHCheckQueryResult.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getReturns**
```objc
-(NSNumber*) getReturnsWithClientId: (NSString*) clientId
    documentId: (NSNumber*) documentId
        completionHandler: (void (^)(NSArray<ACHReturn>* output, NSError* error)) handler;
```

Retrieve returns for a specific Check.

### Example 
```objc
ACHConfiguration *apiConfig = [ACHConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: basicAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* clientId = @"clientId_example"; // 
NSNumber* documentId = @789; // 

ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];

// Retrieve returns for a specific Check.
[apiInstance getReturnsWithClientId:clientId
              documentId:documentId
          completionHandler: ^(NSArray<ACHReturn>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling ACHChecksApi->getReturns: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***|  | 
 **documentId** | **NSNumber***|  | 

### Return type

[**NSArray<ACHReturn>***](ACHReturn.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getSettlements**
```objc
-(NSNumber*) getSettlementsWithClientId: (NSString*) clientId
    documentId: (NSNumber*) documentId
        completionHandler: (void (^)(NSArray<ACHSettlement>* output, NSError* error)) handler;
```

Retrieve Settlements for a specific Check. Usually there is at most one settlement per check.

### Example 
```objc
ACHConfiguration *apiConfig = [ACHConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: basicAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* clientId = @"clientId_example"; // 
NSNumber* documentId = @789; // 

ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];

// Retrieve Settlements for a specific Check. Usually there is at most one settlement per check.
[apiInstance getSettlementsWithClientId:clientId
              documentId:documentId
          completionHandler: ^(NSArray<ACHSettlement>* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling ACHChecksApi->getSettlements: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***|  | 
 **documentId** | **NSNumber***|  | 

### Return type

[**NSArray<ACHSettlement>***](ACHSettlement.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validateCheck**
```objc
-(NSNumber*) validateCheckWithClientId: (NSString*) clientId
    values: (ACHValidateCheckParams*) values
        completionHandler: (void (^)(ACHValidationResult* output, NSError* error)) handler;
```

Validates a check. This usually involves verifying the account exists and that there are no unpaid items (a.k.a. bounced checks) associated with the account. This is configured in the Global Gateway's client manager.

### Example 
```objc
ACHConfiguration *apiConfig = [ACHConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: basicAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* clientId = @"clientId_example"; // 
ACHValidateCheckParams* values = [[ACHValidateCheckParams alloc] init]; //  (optional)

ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];

// Validates a check. This usually involves verifying the account exists and that there are no unpaid items (a.k.a. bounced checks) associated with the account. This is configured in the Global Gateway's client manager.
[apiInstance validateCheckWithClientId:clientId
              values:values
          completionHandler: ^(ACHValidationResult* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling ACHChecksApi->validateCheck: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***|  | 
 **values** | [**ACHValidateCheckParams***](ACHValidateCheckParams*.md)|  | [optional] 

### Return type

[**ACHValidationResult***](ACHValidationResult.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **voidCheck**
```objc
-(NSNumber*) voidCheckWithClientId: (NSString*) clientId
    documentId: (NSNumber*) documentId
        completionHandler: (void (^)(NSError* error)) handler;
```

Delete a specific Check transaction.

### Example 
```objc
ACHConfiguration *apiConfig = [ACHConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: basicAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


NSString* clientId = @"clientId_example"; // 
NSNumber* documentId = @789; // 

ACHChecksApi*apiInstance = [[ACHChecksApi alloc] init];

// Delete a specific Check transaction.
[apiInstance voidCheckWithClientId:clientId
              documentId:documentId
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling ACHChecksApi->voidCheck: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **clientId** | **NSString***|  | 
 **documentId** | **NSNumber***|  | 

### Return type

void (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

