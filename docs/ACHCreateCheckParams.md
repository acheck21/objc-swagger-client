# ACHCreateCheckParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**individualName** | **NSString*** |  | [optional] 
**accountType** | **NSString*** |  | 
**amount** | **NSNumber*** |  | 
**routingNumber** | **NSString*** |  | 
**accountNumber** | **NSString*** |  | 
**checkNumber** | **NSString*** |  | 
**micr** | **NSString*** |  | [optional] 
**entryClass** | **NSString*** |  | 
**addenda** | **NSArray&lt;NSString*&gt;*** |  | [optional] 
**frontImageEncoded** | **NSString*** |  | [optional] 
**rearImageEncoded** | **NSString*** |  | [optional] 
**attachments** | [**NSArray&lt;ACHCreateAttachmentParams&gt;***](ACHCreateAttachmentParams.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


