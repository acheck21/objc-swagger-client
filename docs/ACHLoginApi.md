# ACHLoginApi

All URIs are relative to *https://gateway.acheck21.com/GlobalGateway*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getCurrentUser**](ACHLoginApi.md#getcurrentuser) | **GET** /api/v1/login | Returns currently logged in valid UserName
[**login**](ACHLoginApi.md#login) | **POST** /api/v1/login | Login and generate cookie for future authorization.


# **getCurrentUser**
```objc
-(NSNumber*) getCurrentUserWithCompletionHandler: 
        (void (^)(NSString* output, NSError* error)) handler;
```

Returns currently logged in valid UserName

### Example 
```objc
ACHConfiguration *apiConfig = [ACHConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: basicAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];



ACHLoginApi*apiInstance = [[ACHLoginApi alloc] init];

// Returns currently logged in valid UserName
[apiInstance getCurrentUserWithCompletionHandler: 
          ^(NSString* output, NSError* error) {
                        if (output) {
                            NSLog(@"%@", output);
                        }
                        if (error) {
                            NSLog(@"Error calling ACHLoginApi->getCurrentUser: %@", error);
                        }
                    }];
```

### Parameters
This endpoint does not need any parameter.

### Return type

**NSString***

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **login**
```objc
-(NSNumber*) loginWithLogin: (ACHCredentials*) login
        completionHandler: (void (^)(NSError* error)) handler;
```

Login and generate cookie for future authorization.

### Example 
```objc
ACHConfiguration *apiConfig = [ACHConfiguration sharedConfig];
// Configure HTTP basic authorization (authentication scheme: basicAuth)
[apiConfig setUsername:@"YOUR_USERNAME"];
[apiConfig setPassword:@"YOUR_PASSWORD"];


ACHCredentials* login = [[ACHCredentials alloc] init]; //  (optional)

ACHLoginApi*apiInstance = [[ACHLoginApi alloc] init];

// Login and generate cookie for future authorization.
[apiInstance loginWithLogin:login
          completionHandler: ^(NSError* error) {
                        if (error) {
                            NSLog(@"Error calling ACHLoginApi->login: %@", error);
                        }
                    }];
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **login** | [**ACHCredentials***](ACHCredentials*.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

