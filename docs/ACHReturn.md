# ACHReturn

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documentId** | **NSNumber*** |  | 
**traceNumber** | **NSString*** |  | [optional] 
**amount** | **NSNumber*** |  | 
**receivedOn** | **NSDate*** |  | 
**returnedOn** | **NSDate*** |  | [optional] 
**returnCode** | **NSString*** |  | [optional] 
**returnMessage** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


