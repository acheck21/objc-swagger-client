# ACHServerException

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exceptionMessage** | **NSString*** |  | [optional] 
**exceptionType** | **NSString*** |  | [optional] 
**stackTrace** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


