# ACHSettlement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**documentId** | **NSNumber*** |  | 
**traceNumber** | **NSString*** |  | [optional] 
**amount** | **NSNumber*** |  | 
**receivedOn** | **NSDate*** |  | 
**settledOn** | **NSDate*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


