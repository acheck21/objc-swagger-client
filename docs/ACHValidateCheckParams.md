# ACHValidateCheckParams

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**individualName** | **NSString*** |  | [optional] 
**amount** | **NSNumber*** |  | 
**routingNumber** | **NSString*** |  | 
**accountNumber** | **NSString*** |  | 
**checkNumber** | **NSString*** |  | 
**micr** | **NSString*** |  | [optional] 
**dlNumber** | **NSString*** |  | [optional] 
**phoneNumber** | **NSString*** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


